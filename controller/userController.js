const httpReq = require('request');
const constants = require("../config/constant");
const User = require('../dao/User.js');
const jwt = require('jsonwebtoken');
var userDao = new User();

module.exports = {
    getUsers: async function(request, response) {
        var responseObj = new Object();
        try {
            let users = await userDao.getUsers();
            responseObj.code = 0;
            responseObj.usersObj = users;
            response.status(200).json(responseObj);
        } catch (e) {
            console.log(e);
        }

    },

    createAccount: async function(request, response) {
        var email = request.body.email;
        var userName = request.body.userName;
        var password = request.body.password;
        var responseObj = new Object();
        if (!email || !userName || !password) {
            responseObj.code = constants.ERROR_CODE_MISSING_PARAM;
            responseObj.description = constants.ERROR_DESC_MISSING_PARAM;
            response.status(400).json(responseObj);
            console.log('Missing Parameter');
            return;
        }
        try {
            await userDao.addUser(email, userName, password);
            var token = jwt.sign({ email: email }, constants.secret, {
                expiresIn: 3600 // expires in 1 hours
            });
            responseObj.code = constants.API_SUCCESS_CODE;
            responseObj.auth = token;
            response.status(200).json(responseObj);
            console.log('User added');
        } catch (e) {
            if (e.message === constants.ERROR_DESC_DUPLICATE_ENTRY) {
                responseObj.code = constants.ERROR_CODE_DUPLICATE_ENTRY;
                responseObj.message = constants.ERROR_DESC_DUPLICATE_ENTRY;
                response.status(400).json(responseObj);
            }
        }
    },

    updateUser: async function(request, response) {
        var email = request.params.email;
        var updateName = request.body.userName;
        var responseObj = new Object();
        if (!email || !updateName) {
            responseObj.code = constants.ERROR_CODE_MISSING_PARAM;
            responseObj.description = constants.ERROR_DESC_MISSING_PARAM;
            response.status(400).json(responseObj);
            console.log('Missing Parameter');
            return;
        }
        try {
            await userDao.updateUser(email, updateName);
            responseObj.code = constants.API_SUCCESS_CODE;
            response.status(200).json(responseObj);
        } catch (e) {
            console.log(e);
        }
    },
    deleteUser: async function(request, response) {
        var email = request.params.email;
        var responseObj = new Object();
        if (!email) {
            responseObj.code = constants.ERROR_CODE_MISSING_PARAM;
            responseObj.description = constants.ERROR_DESC_MISSING_PARAM;
            response.status(400).json(responseObj);
            console.log('Missing Parameter');
            return;
        }
        try {
            await userDao.deleteUser(email);
            responseObj.code = constants.API_SUCCESS_CODE;
            response.status(200).json(responseObj);
        } catch (e) {
            console.log(e);
        }
    }



}