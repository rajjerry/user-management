const httpReq = require('request');
const constants = require("../config/constant");
const jwt = require('jsonwebtoken');
const User = require('../dao/User');

module.exports = {
    authenticateAdmin: function(req, res, next) {
        module.exports.authenticate(req, res, next, constants.secret);
    },

    authenticate: async function(req, res, next, secret) {
        var token = req.get('Authorization') || req.query.Authorization;
        var responseObj = new Object();
        if (token === undefined || token.length < 2) {
            responseObj.code = constants.ERROR_CODE_INVALID_PARAM;
            responseObj.description = constants.ERROR_DESC_INVALID_PARAM;
            return res.status(401).send(responseObj);
        }
        jwt.verify(token, secret,
            async function(err, decoded) {
                console.log(decoded);
                if (err) {
                    responseObj.code = constants.ERROR_CODE_INVALID_TOKEN;
                    responseObj.description = constants.ERROR_DESC_INVALID_TOKEN;
                    return res.status(401).send(responseObj);
                } else {
                    req.param.payload = decoded;

                    if (constants.secret == secret && decoded) {
                        console.log('MATCH');
                        var userDao = new User();
                        let isValid = await userDao.validateUser(decoded.email);
                        if (isValid) {
                            next();
                            return;
                        } else {
                            responseObj.code = constants.ERROR_CODE_INVALID_TOKEN;
                            responseObj.description = constants.ERROR_DESC_INVALID_TOKEN;
                            return res.status(401).send(responseObj);
                        }
                    }
                }
            })
    }
}