module.exports = {
    ERROR_CODE_MISSING_PARAM: 1,
    ERROR_DESC_MISSING_PARAM: "Missing parameter",
    ERROR_CODE_INTERNAL_ERROR: 2,
    ERROR_DESC_INTERNAL_ERROR: "Internal server error, Please try later",
    ERROR_CODE_DUPLICATE_ENTRY: 3,
    ERROR_DESC_DUPLICATE_ENTRY: "Duplicate entry",
    ERROR_CODE_INVALID_PARAM: 4,
    ERROR_DESC_INVALID_PARAM: "Invalid parameter",
    ERROR_CODE_INVALID_TOKEN: 5,
    ERROR_DESC_INVALID_TOKEN: "Invalid token",
    API_SUCCESS_CODE: 0,
    secret: 'superStar'
}