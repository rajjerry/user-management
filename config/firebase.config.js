const firebase = require("firebase-admin");

firebase.initializeApp({
    credential: firebase.credential.cert(__dirname + '/' + 'funkyjunction-99047-firebase-adminsdk-cepai-996adcb3a2.json'),
    databaseURL: "https://funkyjunction-99047.firebaseio.com"
});

module.exports = {
    UserRef: firebase.database().ref('Users')
}