const app = require('../app');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
const expect = require('chai').expect;

chai.use(chaiHttp);

describe('All API CALLS', () => {

    it('should GET USERS CALL', (done) => {
        chai.request(app)
            .get('/getUsers').end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                done();
            });
    });

    it('should Add user POST CALL', (done) => {
        chai.request(app).
        post('/createAccount').send({ email: 'rajjari@12345.com', userName: 'rajert', password: 'raj123' })
            .end((err, res) => {
                if (res.status === 400) {
                    console.log('Expected error :::::::::::::::: user already exist');
                    done();
                } else {
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('auth');
                    expect(res.body).to.have.property('code');
                    done();
                }

            });
    });

    it('should updatee the user put call', (done) => {
        chai.request(app).
        put('/User/rajjari@12345.com').send({ userName: 'raj123' })
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('code');
                done();
            });
    });

    it('should delete the user call DELETE', (done) => {
        chai.request(app).
        delete('/User/rajjari@12345.com')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('code');
                done();
            });
    });
});