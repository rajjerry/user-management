const express = require('express');
const app = express();
const bodyParser = require('body-parser');
// app.get('/', function(req, res) {
//     res.sendFile(__dirname + "/views/index.html");
// });

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use(express.static(__dirname));
require('./app.routes.js')(app);

var server = app.listen(8090, function() {
    var host = server.address().address;
    var port = server.address().port;
    console.log(`App listening at http://${host}:${port}`);
});

module.exports = app;