const userRef = require("../config/firebase.config").UserRef;
const constants = require("../config/constant");

function User() {}
module.exports = User;

User.prototype.addUser = async function(email, userName, password) {
    let isExist = await this.validateUser(email);
    if (!isExist) {
        var userObj = new Object();
        userObj.email = email.toLowerCase();
        userObj.userName = userName;
        userObj.password = password;
        userRef.push(userObj);
    } else {
        throw new Error(constants.ERROR_DESC_DUPLICATE_ENTRY);
    }
}

User.prototype.validateUser = async function(email) {
    var userEmail = email.toLowerCase();
    let allUserData = await userRef.orderByChild('email').equalTo(userEmail).once('value');
    if (allUserData.val()) {
        return true;
    } else {
        return false;
    }
}

User.prototype.getUsers = async function() {
    let allUserData = await userRef.once('value');
    let usersArray = [];
    if (allUserData.val()) {
        allUserData.forEach((userData) => {
            usersArray.push(userData.val());
        })
    }
    return usersArray;
}

User.prototype.updateUser = async function(email, updateName) {
    var userEmail = email.toLowerCase();
    let allUserData = await userRef.orderByChild('email').equalTo(userEmail).once('value');
    if (allUserData.val()) {
        allUserData.forEach(async function(userData) {
            var userId = userData.key;
            await userRef.child(userId).child('userName').set(updateName);
        })
        console.log('Name set');
    }
}

User.prototype.deleteUser = async function(email) {
    var userEmail = email.toLowerCase();
    let allUserData = await userRef.orderByChild('email').equalTo(userEmail).once('value');
    if (allUserData.val()) {
        allUserData.forEach(async function(userData) {
            var userId = userData.key;
            await userRef.child(userId).remove();
        })
        console.log('Delete user');
    }
}