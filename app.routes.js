const userController = require('./controller/userController');
const authController = require('./controller/authController');


module.exports = function(app) {
    app.get('/getUsers', userController.getUsers);
    app.post('/createAccount', userController.createAccount);
    app.use('/userAuth', authController.authenticateAdmin);
    app.put('/User/:email', userController.updateUser);
    app.delete('/User/:email', userController.deleteUser);
}